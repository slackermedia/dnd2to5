#!/usr/bin/env python3

import argparse
import sys
import random

def getOptions(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-a", "--ac", type=int, default=0, help="Armor class")
    parser.add_argument("-d", "--hd", type=int, default=0,help="Hit die")
    parser.add_argument("-f", "--fly", type=int, default=0,help="Fly speed")
    parser.add_argument("-g", "--glide", type=int, default=0,help="Glide speed")
    parser.add_argument("-i", "--int", type=int, dest='inl',default=0,help="INT number (one digit, not a range)")
    parser.add_argument("-l", "--shield", type=int, default=0,help="Natural armor, armor, or shield (provide your best guess)")
    parser.add_argument("-m", "--magic",dest='magic',action='store_true', help="Magic resistance (default: False)")
    parser.add_argument("-o", "--morale", type=int, dest='cha', default=0, help="Morale (one digit, not a range)")
    parser.add_argument("-r", "--run", type=int, default=0,help="Run speed")
    parser.add_argument("-s", "--swim", type=int, default=0,help="Swim speed")
    parser.add_argument("-v", "--verbose",dest='verbose',action='store_true', help="Verbose mode.")
    parser.add_argument("-w", "--walk", type=int, default=0,help="Walk speed")

    options = parser.parse_args(args)
    return options

options = getOptions(sys.argv[1:])

def calculator(ac,hd,verbose,walk,run,glide,fly,swim,magic):
    if ac < 23:
        nac=19-ac
    else:
        # conversion doc doesnt say what to do over AC 22
        nac=19-23

    nattack=(hd/2)+2
    # conversion doc doesnt say what to do over +12
    if nattack > 12:
        nattack=12

    nsave=8+(hd/2)
    # conversion doc doesnt say what to do over +20
    if nsave > 20:
        nsave=20

    speed = [ walk,run,glide,fly,swim ]

    for idx,item in enumerate(speed):
        speed[idx]=int(item)*5

    return { 'AC':nac,'Attack':nattack,'Save':nsave,'Walk':speed[0],'Run':speed[1],'Glide':speed[2],'Fly':speed[3],'Swim':speed[4],'Magic':magic }

def attributes( hd,shield,inl,cha,stats,verbose):
    dexadj=stats['AC']-shield

    standard=[ 15,14,13,12,10,8 ]

    #dex=(dexadj/2)+10
    if dexadj > 10:
        dex=(dexadj/2)+10
    elif dexadj < 8:
        dex=10-(dex/2)
    else:
        dex=10

    stn=(stats['Attack']*2)+10

    # INT
    '''
    0 Non-intelligent or not ratable
    1 Animal intelligence
    2-4 Semi-intelligent
    5-7 Low intelligence
    8-10 Average (human) intelligence
    11-12 Very intelligent
    13-14 Highly intelligent
    15-16 Exceptionally intelligent
    17-18 Genius
    19-20 Supra-genius
    21+ Godlike intelligence
    '''

    if inl > 10:
        inl=(inl/2)+10
    elif inl < 8:
        inl=10-(inl/2)
    else:
        inl=10

    # CHA
    '''
    2-4 Unreliable
    5-7 Unsteady
    8-10 Average
    11-12 Steady
    13-14 Elite
    15-16 Champion
    17-18 Fanatic
    19-20 Fearless
    '''

    if cha > 10:
        cha=(cha/2)+10
    elif cha < 8:
        cha=10-(cha/2)
    else:
        cha=10

    for i in (dex,stn,inl,cha):
        if i in standard:
            standard.remove(i)

    hi = random.choice(standard)
    lo = random.choice(standard)

    if options.magic:
        wis=hi
        con=lo
    else:
        wis=lo
        con=hi

    return { 'STR':int(stn),'DEX':int(dex),'CON':int(con),'INT':int(inl),'WIS':int(wis),'CHA':int(cha) }

## execute
stats = calculator(options.ac, options.hd, options.verbose, options.walk, options.run, options.glide, options.fly, options.swim, options.magic)

att = attributes( options.hd,options.shield,options.inl,options.cha,stats,options.verbose)

print('STR: ' + str(att['STR']) + ' (' + str( int((att['STR']-10)/2)) + ')' )
print('DEX: ' + str(att['DEX']) + ' (' + str( int((att['DEX']-10)/2)) + ')' )
print('CON: ' + str(att['CON']) + ' (' + str( int((att['CON']-10)/2)) + ')' )
print('INT: ' + str(att['INT']) + ' (' + str( int((att['INT']-10)/2)) + ')' )
print('WIS: ' + str(att['WIS']) + ' (' + str( int((att['WIS']-10)/2)) + ')' )
print('CHA: ' + str(att['CHA']) + ' (' + str( int((att['CHA']-10)/2)) + ')' )

print( 'AC: ' + str(stats['AC']) )
print( 'Attack: ' + str( int(stats['Attack'])) )
print( 'Save: ' + str( int(stats['Save'])) )
print( 'Walk speed: ' + str(stats['Walk']) + " feet")
print( 'Run speed: ' + str(stats['Run']) + " feet")
print( 'Glide speed: ' + str(stats['Glide']) + " feet")
print( 'Fly: ' + str(stats['Fly']) + " feet")
print( 'Swim: ' + str(stats['Swim']) + " feet")

if options.magic:
    print( 'Magic resistance: Advantage')

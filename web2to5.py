#!/usr/bin/env python3

from flask import Flask, url_for, request, render_template
import argparse
import sys
import random

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit', methods=['POST', 'GET'])
def submit():
    error = None
    if request.method == 'POST':
        ac = int(request.form['ac'])
        hd = int(request.form['hd'])
        fly = int(request.form['fly'])
        glide = int(request.form['glide'])
        inl = int(request.form['inl'])
        shield = int(request.form['shield'])
        magic = bool(request.form['magic'])
        cha = int(request.form['morale'])
        run = int(request.form['run'])
        swim = int(request.form['swim'])
        walk = int(request.form['walk'])

        stats = calculator(ac,hd,walk,run,glide,fly,swim,magic)
        att = attributes( hd,shield,inl,cha,stats )

        return render_template('converted.html', AC=int(stats['AC']), Attack=int(stats['Attack']), Walk=int(stats['Walk']), Run=int(stats['Run']), Glide=int(stats['Glide']),Fly=int(stats['Fly']),Swim=int(stats['Swim']),Save=int(stats['Save']),STR=int(att['STR']), DEX=int(att['DEX']),CON=int(att['CON']),INT=int(att['INT']), WIS=int(att['WIS']),CHA=int(att['CHA']) )

    else:
        error = 'Invalid submission'
        return render_template('index.html', error=error)

def calculator(ac,hd,walk,run,glide,fly,swim,magic):
    if ac < 23:
        nac=19-ac
    else:
        # conversion doc doesnt say what to do over AC 22
        nac=19-23

    nattack=(hd/2)+2
    # conversion doc doesnt say what to do over +12
    if nattack > 12:
        nattack=12

    nsave=8+(hd/2)
    # conversion doc doesnt say what to do over +20
    if nsave > 20:
        nsave=20

    speed = [ walk,run,glide,fly,swim ]

    for idx,item in enumerate(speed):
        speed[idx]=int(item)*5

    ## stat results

    return { 'AC':nac,'Attack':nattack,'Save':nsave,'Walk':speed[0],'Run':speed[1],'Glide':speed[2],'Fly':speed[3],'Swim':speed[4],'Magic':magic }


def attributes( hd,shield,inl,cha,stats ):
    dexadj=stats['AC']-shield

    standard=[ 15,14,13,12,10,8 ]

    #dex=(dexadj/2)+10
    if dexadj > 10:
        dex=(dexadj/2)+10
    elif dexadj < 8:
        dex=10-(dexadj/2)
    else:
        dex=10

    stn=(stats['Attack']*2)+10

    # INT
    '''
    0 Non-intelligent or not ratable
    1 Animal intelligence
    2-4 Semi-intelligent
    5-7 Low intelligence
    8-10 Average (human) intelligence
    11-12 Very intelligent
    13-14 Highly intelligent
    15-16 Exceptionally intelligent
    17-18 Genius
    19-20 Supra-genius
    21+ Godlike intelligence
    '''

    if inl > 10:
        inl=(inl/2)+10
    elif inl < 8:
        inl=10-(inl/2)
    else:
        inl=10

    # CHA
    '''
    2-4 Unreliable
    5-7 Unsteady
    8-10 Average
    11-12 Steady
    13-14 Elite
    15-16 Champion
    17-18 Fanatic
    19-20 Fearless
    '''

    if cha > 10:
        cha=(cha/2)+10
    elif cha < 8:
        cha=10-(cha/2)
    else:
        cha=10

    for i in (dex,stn,inl,cha):
        if i in standard:
            standard.remove(i)

    hi = random.choice(standard)
    lo = random.choice(standard)

    if stats['Magic']:
        wis=hi
        con=lo
    else:
        wis=lo
        con=hi

    return { 'STR':int(stn),'DEX':int(dex),'CON':int(con),'INT':int(inl),'WIS':int(wis),'CHA':int(cha) }
    


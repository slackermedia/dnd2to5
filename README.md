# DND 2e to 5e Converter

This is a Python script you can use to get the mostly-programmatic conversion work done when porting a monster from Dungeons & Dragons 2e to 5e.

This is based on the [Conversions to 5th Edition D&D](https://media.wizards.com/2015/downloads/dnd/DnD_Conversions_1.0.pdf) document published by Wizards of the Coast. This document itself literally states "Conversion of D&D material is more art than science" so this script necessarily is imprecise, but the goal is to produce:

* An accurate conversion of AC and speed values, as specified by the conversion document
* A reasonable STR and DEX value based on the converted AC and Attack bonus values 
* A starting point array of remaining attributes: WIS based on magic potential, INT based on original INT value, and CHA based on original Morale value.

## How do I use this?

Eventually, this converter will be used as the back-end for a web app. 

For now, you can run the script from a terminal, as long as you have Python 3 installed.

Use the ``--help`` option for up-to-date options.

Values for each option should be taken from any 2e bestiary, such as the Monstrous Compendium or the Monstrous Manual.

## Do I need to use this? 

Many monsters from 2e have already been ported to 5e by Wizards of the Coast in the Monster Manual, Volo's Guide to Monsters, Mordenkainen's Tome of Foes, and so on. If they don't exist there, then you might pick up a copy of the [Tome of Beasts](https://koboldpress.com/kpstore/product/tome-of-beasts-for-5th-edition/) from Kobold Press, or [Fifth Edition Foes](https://froggodgames.com/frogs/product/fifth-edition-foes/) from Frog God Games, and you're likely to find something close enough to what you're looking for.

## What about the other 66%

This script gets the math done for you, but you still have to port over skills and feats and other special powers.

The best way to do that is to read the conversion guide. There are no plans to ever make this script handle that part of the work.

## Bugs

There may well be statistical optimizations that could be made here. Probability to hit between THAC0 and the d20 system is complex, so if you have better ways of converting, let me know here on Gitlab or by emailing me: klaatu at the domain of member.fsf.org.


